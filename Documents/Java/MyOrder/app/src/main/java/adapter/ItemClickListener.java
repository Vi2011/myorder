package adapter;

import android.view.View;

/**
 * Created by bathu on 11/26/2017.
 */

public interface ItemClickListener {
    void onItemClick(View v, int position);
}
