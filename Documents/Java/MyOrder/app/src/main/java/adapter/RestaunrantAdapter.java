package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import filter.RestaurantFilter;
import model.Restaurant;
import thjava.myorder.R;

/**
 * Created by bathu on 11/25/2017.
 */

public class RestaunrantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> implements Filterable {

    private ArrayList<Restaurant> restaurantArrayList, filterList;
    private Context context;
    private RestaurantFilter restaurantFilter;

    public RestaunrantAdapter(ArrayList<Restaurant> restaurantArrayList, Context context) {
        this.restaurantArrayList = restaurantArrayList;
        this.context = context;
        this.filterList=restaurantArrayList;
    }


    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.restaurant_item_layout,parent,false);

        return new RestaurantViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, int position) {
        holder.txtRestaurantName.setText(restaurantArrayList.get(position).getName());
        holder.txtRestaurantAddress.setText(restaurantArrayList.get(position).getAddress());
        holder.txtRestaurantDiscription.setText(restaurantArrayList.get(position).getDiscription());

        String url = restaurantArrayList.get(position).getImage();
        Picasso.with(context).load(url).into(holder.imgRestaurantLogo);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Toast.makeText(context,restaurantArrayList.get(position).getName(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return restaurantArrayList.size();
    }

    public void setRestaurants(ArrayList<Restaurant> restaurants){
        this.restaurantArrayList=restaurants;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if(restaurantFilter==null){
            restaurantFilter= new RestaurantFilter(filterList,this);
        }
        return restaurantFilter;
    }
}