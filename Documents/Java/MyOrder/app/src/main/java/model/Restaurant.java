package model;

/**
 * Created by bathu on 11/25/2017.
 */

public class Restaurant {
    private String Id;
    private String Name;
    private String Address;
    private String Discription;
    private String Image;

    public Restaurant(String id, String name, String address, String discription, String image) {
        Id = id;
        Name = name;
        Address = address;
        Discription = discription;
        Image = image;
    }

    public String getId() {
        return Id;
    }

    public Restaurant setId(String id) {
        Id = id;
        return this;
    }

    public String getName() {
        return Name;
    }

    public Restaurant setName(String name) {
        Name = name;
        return this;
    }

    public String getAddress() {
        return Address;
    }

    public Restaurant setAddress(String address) {
        Address = address;
        return this;
    }

    public String getDiscription() {
        return Discription;
    }

    public Restaurant setDiscription(String discription) {
        Discription = discription;
        return this;
    }

    public String getImage() {
        return Image;
    }

    public Restaurant setImage(String image) {
        Image = image;
        return this;
    }
}
