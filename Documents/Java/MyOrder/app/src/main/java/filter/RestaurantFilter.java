package filter;

import android.widget.Filter;

import java.util.ArrayList;

import adapter.RestaunrantAdapter;
import model.Restaurant;

/**
 * Created by bathu on 11/26/2017.
 */

public class    RestaurantFilter extends Filter {

    static ArrayList<Restaurant> restaurantCurrentList;
    static RestaunrantAdapter restaunrantAdapter;

    public RestaurantFilter (ArrayList<Restaurant> restaurants, RestaunrantAdapter restaunrantAdapter){
        RestaurantFilter.restaurantCurrentList=restaurants;
        RestaurantFilter.restaunrantAdapter=restaunrantAdapter;
    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults = new FilterResults();

        if(constraint!=null && constraint.length()>0){
            constraint=constraint.toString().toLowerCase();

            ArrayList<Restaurant> restaurantFoundList = new ArrayList<>();

            for(Restaurant restaurant : restaurantCurrentList){

                if(restaurant.getName().toLowerCase().contains(constraint)||
                        restaurant.getDiscription().toLowerCase().contains(constraint)) {
                    restaurantFoundList.add(restaurant);
                }
            }

            filterResults.count=restaurantFoundList.size();
            filterResults.values=restaurantFoundList;

        }else {
            filterResults.count=restaurantCurrentList.size();
            filterResults.values=restaurantCurrentList;
        }

        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        restaunrantAdapter.setRestaurants((ArrayList<Restaurant>) results.values);
    }
}
