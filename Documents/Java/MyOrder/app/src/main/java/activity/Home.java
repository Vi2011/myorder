package activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;

import thjava.myorder.R;
import model.Restaurant;
import adapter.RestaunrantAdapter;

public class Home extends Activity {

    Button btnBack;
    SearchView searchView;
    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView txtHelloUser;
    ArrayList<Restaurant> restaurantArrayList;
    RestaunrantAdapter  restaurantAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        InitComponent();
        InitView();
    }

    private void InitComponent() {
        btnBack = (Button) findViewById(R.id.btnMain_Back);
        searchView = (SearchView) findViewById(R.id.sevMain_Restaurant);
        // Get textview id of search widget
        int id =  searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        // Set search text color
        textView.setTextColor(Color.WHITE);
        textView.setHintTextColor(Color.GRAY);

        toolbar = (Toolbar) findViewById(R.id.toolbarMain);
        recyclerView = (RecyclerView)findViewById(R.id.revRestaurant);
        txtHelloUser =(TextView) findViewById(R.id.txtMain_HelloUser);
    }

    private void InitView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoratio = new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.restaurant_item_divider);
        dividerItemDecoratio.setDrawable(drawable);
        recyclerView.addItemDecoration(dividerItemDecoratio);

        restaurantArrayList = new ArrayList<>();
        restaurantArrayList.add(new Restaurant("2e","My Sushi","177 Trần Xuân Soạn,Quận 7 Hồ Chí Minh","Món Nhật và Sushi","http://static.vietnammm.com/images/restaurants/vn/5NN31PN/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("2f","Pizza Origino","72A Đường số 4, phường Tân Kiểng,Quận 7 Hồ Chí Minh","Italian & Pizza","http://static.vietnammm.com/images/restaurants/vn/NNOPRRO/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("sds","Moto Cafe","72A Đường số 4, phường Tân Kiểng,Quận 7 Hồ Chí Minh","Trà sữa - Café","http://static.vietnammm.com/images/restaurants/vn/50NP353/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("sds","Nhà Hàng OLiu Đỏ","72A Đường số 4, phường Tân Kiểng,Quận 7 Hồ Chí Minh","Món Pháp, Món Châu Âu, Italian & Pizza","http://static.vietnammm.com/images/restaurants/vn/OOPQ0R5/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("sds","Jollibee","72A Đường số 4, phường Tân Kiểng,Quận 7 Hồ Chí Minh","Sandwich, Cơm, Italian & Pizza","http://static.vietnammm.com/images/restaurants/vn/53130R3/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("sds","ThaiExpress","72A Đường số 4, phường Tân Kiểng,Quận 7 Hồ Chí Minh","Món Thái Lan","http://static.vietnammm.com/images/restaurants/vn/N1530N5/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("2e","Sticky Fingers","177 Trần Xuân Soạn,Quận 7 Hồ Chí Minh","Món Mỹ, Món Châu Âu","http://static.vietnammm.com/images/restaurants/vn/Q00QRN3/logo_465x320.png"));
        restaurantArrayList.add(new Restaurant("2e","Mad House","6/1/2 Nguyễn Ư Dĩ, Thảo Điền,Quận 2 Hồ Chí Minh","Sandwich, Món Mỹ, Món Châu Âu","http://static.vietnammm.com/images/restaurants/vn/QNO753O/logo_465x320.png"));
        restaurantAdapter = new RestaunrantAdapter(restaurantArrayList,getApplicationContext());
        recyclerView.setAdapter(restaurantAdapter);

        // Search event
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                restaurantAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }
}
