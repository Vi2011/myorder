package activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {
    private EditText inputName, inputPhone, inputPassword, inputRePassword;
    private Button btnLogin;
    private Button btnRegister;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        inputName = (EditText) findViewById(R.id.edt_name_reg);
        inputPhone = (EditText) findViewById(R.id.edt_phone_reg);
        inputPassword = (EditText) findViewById(R.id.edt_password_reg);
        inputRePassword = (EditText) findViewById(R.id.edt_re-password_reg);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        btnLinkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
            }
        });
    }

    private void submitForm() {
        registerUser(inputName.getText().toString(),
                inputPhone.getText().toString(),
                inputPassword.getText().toString(),
                inputRePassword.getText().toString());
    }

    private void registerUser(final String name, final String phone, final String password, final String repassword) {
        progressDialog.setMessage("Register...");
        showDialog();


    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
