package activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import thjava.menu.MenuItem;
import thjava.menu.MenuRecyclerViewAdapter;
import thjava.menu.ReadJSON;
import thjava.myorder.R;


public class Menu extends AppCompatActivity{

    private List<MenuItem> data = new ArrayList<MenuItem>();
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //TODO: Set restaurant name here
        getSupportActionBar().setTitle("Ten nha hang");

        try {
            // Đọc file: res/raw/company.json và trả về đối tượng Company.
            data = ReadJSON.readFoodJSONFile(this);
        } catch(Exception e)  {
            e.printStackTrace();
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.menu_recyclerView);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        MenuRecyclerViewAdapter menuRecyclerViewAdapter = new MenuRecyclerViewAdapter(data);
        recyclerView.setAdapter(menuRecyclerViewAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_sceen_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        //TODO: set action for Done button here
        String msg = " ";
        if (item.getItemId() == R.id.done) {
            msg = "Done checked! Set Action for this button here!";
        }
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }
}
