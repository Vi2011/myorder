package thjava.menu;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import thjava.myorder.R;

import static android.support.v7.widget.RecyclerView.*;

/**
 * Created by nmdli on 25-Nov-17.
 * Adapter for RecyclerView in Menu screen
 */


public class MenuRecyclerViewAdapter extends RecyclerView.Adapter<MenuRecyclerViewAdapter.RecyclerViewHolder>{

    private List<MenuItem> menuItems = new ArrayList<>();

    public MenuRecyclerViewAdapter(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.recycler_view_item, viewGroup, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {

        holder.name.setText(menuItems.get(position).getName());
        holder.price.setText(menuItems.get(position).getPrice());
        holder.itemNumber.setText("0");
        holder.imageView.setTag(menuItems.get(position).getImageURL());
        new GetImage().execute(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements OnClickListener{

        public TextView name;
        private TextView price;
        private EditText itemNumber;
        private Button btnDelete;
        private Button btnOrder;
        private ImageView imageView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.txt_itemName);
            price = (TextView) itemView.findViewById(R.id.txt_itemPrice);
            itemNumber = (EditText) itemView.findViewById(R.id.edt_itemNumber);
            btnDelete = (Button) itemView.findViewById(R.id.btn_deleteButton);
            btnOrder = (Button) itemView.findViewById(R.id.btn_orderButton);
            imageView = (ImageView) itemView.findViewById(R.id.img_itemImage);

            //set listener for button here
            btnOrder.setOnClickListener(this);
            btnDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            //set action for button here
            int itemNum = Integer.parseInt(itemNumber.getText().toString());

            if (v.getId() == btnDelete.getId()){
                if (itemNum > 0)
                    itemNum -= 1;
                else
                    itemNum =0;

                itemNumber.setText(String.valueOf(itemNum));
            }

            if (v.getId() == btnOrder.getId()){
                itemNum ++;
                itemNumber.setText(String.valueOf(itemNum));
            }

        }
    }
    public class GetImage extends AsyncTask<ImageView, Void, Bitmap> {

        ImageView imageView = null;

        @Override
        protected Bitmap doInBackground(ImageView... imageViews) {
            this.imageView = imageViews[0];
            return getImage((String)imageView.getTag());
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }

        private Bitmap getImage(String url) {

            Bitmap bmp =null;
            try{
                URL ulrn = new URL(url);
                HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
                InputStream is = con.getInputStream();
                bmp = BitmapFactory.decodeStream(is);
                if (null != bmp)
                    return bmp;

            }catch(Exception e){}
            return bmp;
        }
    }
}
