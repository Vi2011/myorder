package thjava.menu;

/**
 * Created by nmdli on 25-Nov-17.
 */

public class MenuItem {
    private int id;
    private String name;
    private String price;
    private String imageURL;
    private int restaurantID;

    public MenuItem(int id, String name, String price, String imageURL, int restaurantid) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imageURL = imageURL;
        this.restaurantID = restaurantid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRestaurantID() {
        return restaurantID;
    }

    public void setRestaurantID(int restaurantID) {
        this.restaurantID = restaurantID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
