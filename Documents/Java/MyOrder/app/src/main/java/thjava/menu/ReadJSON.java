package thjava.menu;

import android.os.AsyncTask;
import android.widget.Toast;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import activity.Menu;
import thjava.myorder.R;

/**
 * Created by XUÂN THANH on 06/12/2017.
 */

public class ReadJSON {

    // Đọc file food.json và chuyển thành đối tượng java.
    public static ArrayList<MenuItem> readFoodJSONFile(Context context) throws IOException,JSONException {

        ArrayList<MenuItem> listMenuItem = new ArrayList<>();

        // Đọc nội dung text của file company.json
        String jsonText = readText(context, R.raw.food);

        // Đối tượng JSONObject gốc mô tả toàn bộ tài liệu JSON.
        JSONObject jsonRootObject = new JSONObject(jsonText);

        // Đưa jsonRootObject vào array object
        JSONArray jsonArray = jsonRootObject.getJSONArray("food");

        // Duyệt từng đối tượng trong Array và lấy giá trị ra
        for(int i=0;i < jsonArray.length();i++)
        {
            // lấy từng đối tượng ra
            JSONObject jsonFood = jsonArray.getJSONObject(i);
            int id= jsonFood.getInt("id");
            String name = jsonFood.getString("name");
            String price = jsonFood.getString("price");
            String imageURL = jsonFood.getString("imageURL");
            int restaurantID= jsonFood.getInt("restaurantID");

            listMenuItem.add(i, new MenuItem(id, name, price, imageURL, restaurantID));
        }
        return listMenuItem;
    }


    // Đọc nội dung text của một file nguồn.
    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br= new BufferedReader(new InputStreamReader(is));
        StringBuilder sb= new StringBuilder();
        String s= null;
        while((  s = br.readLine())!=null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}
